package br.edu.up;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serializador {

	public static void main(String[] args) throws Exception {

		
		Produto p = new Produto();
		p.setId(1L);
		p.setNome("Pneu");
		p.setValor(300.00);
		
		
		FileOutputStream fos = new FileOutputStream("produto.ser");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(p);
		oos.close();
		
		FileInputStream fis = new FileInputStream("produto.ser");
		ObjectInputStream ois = new ObjectInputStream(fis);
		Produto p2 = (Produto) ois.readObject();
		ois.close();
		System.out.println(p2);
		
		
		
	}

}
